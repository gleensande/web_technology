from django.db import models
from django.utils import timezone

class Profile(models.Model):
	key = models.BigIntegerField(primary_key = True)
	login = models.CharField(max_length = 50)
	password = models.CharField(max_length = 50)
	name = models.CharField(max_length = 50)
	email = models.EmailField()
	rating = models.IntegerField()
	image = models.ImageField()

	def __str__(self):
		return self.name

class Question(models.Model):
	key = models.BigIntegerField(primary_key = True)
	header = models.CharField(max_length = 50)
	text = models.TextField()
	p_key = models.ForeignKey('Profile', null = True,  on_delete = models.SET_NULL)
	rating = models.IntegerField()
	publish_time = models.DateTimeField()
	tag = models.ManyToManyField('Tag')

	def publish(self):
		self.publish_time = timezone.now()
		self.save()
	
	def __str__(self):
		return self.header

class Answer(models.Model):
	key = models.BigIntegerField(primary_key = True)
	q_key = models.ForeignKey('Question', on_delete = models.CASCADE)
	text = models.TextField()
	p_key = models.ForeignKey('Profile', null = True, on_delete = models.SET_NULL)
	rating = models.IntegerField()
	correct = models.BooleanField()
	publish_time = models.DateTimeField()

	def publish(self):
		self.publish_time = timezone.now()
		self.save()
	
	def __str__(self):
		return str(self.key)

class Tag(models.Model):
	key = models.BigIntegerField(primary_key = True)
	name = models.CharField(max_length = 50)
	create = models.IntegerField()
	click = models.IntegerField()
	rating = models.IntegerField()
	
	def __str__(self):
		return self.name
	
