from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.shortcuts import render
from .models import Profile
from .models import Tag
from .models import Question
from .models import Answer

def index(request):
    questions, answers_count = index_manager(request)
    return render(request, 'question/index_previews.html', {'questions': questions, 'answers_count': answers_count})

def hot(request):
    questions, answers_count = hot_manager(request)
    return render(request, 'question/hot_previews.html', {'questions': questions, 'answers_count': answers_count})

def settings(request):
    return render(request, 'question/settings.html', {})

def ask(request):
    return render(request, 'question/ask.html', {})

def found(request):
    return render(request, 'question/found.html', {})

def question(request, question_key):
    question, answers = question_manager(request, question_key)
    return render(request, 'question/question.html', {'question': question, 'answers': answers})

def authorisation(request):
    return render(request, 'question/authorisation.html', {})

def registration(request):
    return render(request, 'question/registration.html', {})

### managers and additional functions ###

def index_manager(request):
    questions = Question.objects.order_by('-publish_time')
    answers_count = answer_counter
    return questions, answers_count

def hot_manager(request):
    questions = Question.objects.order_by('rating')
    answers_count = answer_counter
    return questions, answers_count

def answer_counter(questions):  #TODO: maybe use special field to count answers in question?
    answers_count = []
    for question in questions:
        answers = Answer.objects.filter(q_key=question.key)
        answers_count.append(answers.count())
    return answers_count

def question_manager(request, question_key):
    question = Question.objects.get(key=question_key)
    answers = Answer.objects.filter(q_key=question_key)
    return question, answers
